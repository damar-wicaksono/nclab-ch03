#nclab-ch03: Exercise Session

Materials related to the Simulation Session of Chapter 03 of 
the Nuclear Computation Lab course given at PSI

The theme of the series in this exercise is basic limiting phenomena
in nuclear engineering thermal-hydraulics evaluated using a 
system thermal-hydraulics code: TRACE. It consists of:

 1. Vessel blowdown which involves choking
 2. Single tube flooding which involves CCFL
 3. Boiling heated channel which involves critical heat flux