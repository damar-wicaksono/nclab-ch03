#!/bin/env python
# -*- coding: utf-8 -*-
# title         : makeHeatedChannelPlot.py
# author        : WD41, LRS/EPFL/PSI
# date          : November 2015
# purpose       : python3 script to make relevant plots of Exercise 3.3
#               : Thermal-Hydraulics - Heated Channel
#               : Nuclear Computational Lab, Autum 2015
# usage         : > python3 makeHeatedChannelPlot.py 
#               : make sure the extracted trace variables exist as csv files
# py_version    : 3.4.3 (Anaconda Python Distribution 2.1.0 (64bit),
#               : https://store.continuum.io/cshop/anaconda
#-----------------------------------------------------------------------------

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

__author__ = "Damar Wicaksono"

# Global matplotlib setting
mpl.rc("legend", fancybox=True)
mpl.rc("legend", fontsize=12)


def get_data(np_data, timeSet: float):
    for i, time in enumerate(np_data[:,0]):
        if time > timeSet:
           data = np_data[i,1:]
           break
           
    return data
    
    
def node_to_level(node_size: float, node_numbers: int, offset=0.0):
    levels = []
    for i in range(node_numbers):
        levels.append(offset+i*node_size+node_size/2)
    
    return np.array(levels)
    
        
def plot_axial(np_data, time1: float, time2: float,
               axial_levels, 
               ylims: list, ylabel: str, 
               plot_title:str, filename: str):
    
    high_mass_data = get_data(np_data, time1)
    low_mass_data = get_data(np_data, time2)
    
    fig = plt.figure(figsize=(10,8))
    ax1 = fig.add_subplot(111)
    ax1.plot(axial_levels, high_mass_data, label=r"High Mass Flow = 60.0 [kg.s$^{-1}$]")
    ax1.plot(axial_levels, low_mass_data, label=r"Low Mass Flow = 10.0 [kg.s$^{-1}$]")
    
    plot_decorate(ax1, [0.0, 4.0], ylims)

    plt.grid()
    plt.xlabel("Axial Level [m]")
    plt.ylabel(ylabel)
    plt.title(plot_title)
    plt.legend(loc="upper left")
    plt.savefig(filename)
    
def plot_level(level_data, filename: str):
    
    fig = plt.figure(figsize=(10, 8))
    ax1 = fig.add_subplot(111)
    ax1.plot(level_data[:,0], level_data[:,1])
    
    plot_decorate(ax1, [0.0, 400.0], [0.0, 4.0])

    plt.grid()
    plt.xlabel("Time [s]")
    plt.ylabel("Collapsed Water Level [m]")
    plt.title("Collapsed Water Level")
    plt.savefig(filename)
    
def plot_fluid_temp(np_data, time: float, axial_levels, 
                    ylims: list, ylabel: str, plot_title: str,
                    filename: str):
    
    sat_temp = get_data(np_data[0], time)
    liq_temp = get_data(np_data[1], time)
    vap_temp = get_data(np_data[2], time)
    
    fig = plt.figure(figsize=(10,8))
    ax1 = fig.add_subplot(111)
    ax1.plot(axial_levels, sat_temp, label="Saturation")
    ax1.plot(axial_levels, liq_temp, label="Liquid")
    ax1.plot(axial_levels, vap_temp, label="Vapor")
    
    plot_decorate(ax1, [0.0, 4.0], ylims)
    
    plt.grid()
    plt.xlabel("Axial Level [m]")
    plt.ylabel(ylabel)
    plt.title(plot_title)
    plt.legend(loc="upper left")
    plt.savefig(filename)
    
def plot_decorate(axis, xlims, ylims):
    axis.spines["right"].set_visible(False)
    axis.spines["top"].set_visible(False)
    axis.spines["left"].set_position(("outward", 10))
    axis.spines["bottom"].set_position(("outward", 10))
    axis.set_xlim(xlims)
    axis.set_ylim(ylims)

def main():
    
    # Plot Axial Void Fraction
    alpha_data = np.loadtxt("heatedChannel_alpn.csv", skiprows=2, delimiter=",")
    ax_levels = node_to_level(0.2, 20)
    plot_axial(alpha_data, 150., 375., ax_levels, [0.0, 1.0], 
               "Void Fraction [-]", "Channel Void Fraction",
               "axialVoid.png")
    
    # Plot Collapsed Water Level
    level_data = np.loadtxt("heatedChannel_sv2.csv", skiprows=2, delimiter=",")
    plot_level(level_data, "collapsedLevel.png")
    
    # Load Fluid Temperature Data
    sattemp_data = np.loadtxt("heatedChannel_tsat.csv", skiprows=2, delimiter=",")
    liqtemp_data = np.loadtxt("heatedChannel_tln.csv", skiprows=2, delimiter=",")
    vaptemp_data = np.loadtxt("heatedChannel_tvn.csv", skiprows=2, delimiter=",")
    
    # Plot Fluid Temperature at High Mass Flow
    plot_fluid_temp([sattemp_data, liqtemp_data, vaptemp_data], 
                    150.0, ax_levels, [550.0, 650.0], 
                    "Temperature [K]", "Fluid Temperature at High Mass Flow",
                    "fluidTempHighMass.png")
        
    # Plot Fluid Temperature at Low Mass Flow
    plot_fluid_temp([sattemp_data, liqtemp_data, vaptemp_data], 
                    350.0, ax_levels, [550.0, 650.0], 
                    "Temperature [K]", "Fluid Temperature at Low Mass Flow",
                    "fluidTempLowMass.png")
    
    # Plot Axial Vapor Velocity
    vapvel_data = np.loadtxt("heatedChannel_vvn.csv", skiprows=2, delimiter=",")
    plot_axial(vapvel_data, 150., 350., ax_levels, [0.0, 30.0], 
               "Velocity [m/s]", "Vapor Velocity along the Channel", 
               "vaporVel.png")
    
    # Plot Cladding Temperature
    cladtemp_data = np.loadtxt("heatedChannel_tsurfo.csv", skiprows=2, delimiter=",")
    ax_levels = node_to_level(0.2, 18, 0.2)
    plot_axial(cladtemp_data, 150.0, 350.0, ax_levels, [500.0, 1200.0], 
               "Temperature [K]", "Cladding Temperature",
               "claddingTemp.png")
    
    # Plot Centerline Temperature
    cladtemp_data = np.loadtxt("heatedChannel_rftn.csv", skiprows=2, delimiter=",")
    plot_axial(cladtemp_data, 150.0, 350.0, ax_levels, [2600.0, 3100.0], 
               "Temperature [K]", "Centerline Temperature",
               "centerlineTemp.png")
     
     
if __name__ == "__main__":
    main()