#!/bin/bash

for ccfl_case in "singleTubeFloodingCCFL" "singleTubeFloodingNoCCFL"
do
    for ext in "dif" "echo" "tpr" "msg" "out"
    do
        filename="$ccfl_case.$ext"
        rm $filename
    done
done
