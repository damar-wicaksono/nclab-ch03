#!/bin/bash

if [ "$1" == "-h" ] || [ "$1" == "--help" ] ; then
    echo "Usage: `basename $0` <trcxtv.xtv> <tracevars.inp>"
    exit 0
fi

trcxtv=$(echo $1 | cut -f 1 -d ".")
cat $2 | sed s/%tracin/$trcxtv/ | aptplot_v6.5.2_inst01.sh -batch - -nowin
