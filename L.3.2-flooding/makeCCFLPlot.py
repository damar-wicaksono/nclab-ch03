#!/usr/env python
# -*- coding: utf-8 -*-
# title         : makeCCFLPlot.py
# author        : WD41, LRS/EPFL/PSI
# date          : November 2015
# purpose       : python3 script to make relevant plots of Exercise 3.2
#               : Thermal-Hydraulics - Single Tube Flooding
#               : Nuclear Computational Lab, Autum 2015
# usage         : > python3 makeCCFLPlot.py 
# prereq        : make sure the extracted trace variables exist as csv files
#               : - ./singleTubeFloodingCCFL.csv
#               : - ./singleTubeFloodingNoCCFL.csv
# py_version    : 3.4.3 (Anaconda Python Distribution 2.1.0 (64bit),
#               : https://store.continuum.io/cshop/anaconda
#-----------------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt

__author__ = "Damar Wicaksono"


def superficial_velocity(mass_flow, tube_diam, rho):
    """Calculate the superficial velocity of flow in a tube
    
    From a given mass flow rate, tube diameter, and the phasic density
    
    :param mass_flow: the phasic mass flow rate
    :param tube_diam: the diameter of the tube
    :param rho: the phasic density
    :return: the superficial velocity
    """
    from math import pi
    flow_area = pi*tube_diam**2*0.25
    sup_velocity = mass_flow/flow_area/rho
    
    return sup_velocity
    
    
def nondim_factor(rho_l, rho_g, tube_diam, g=9.81):
    """Calculate the factor to make a dimensionless superficial velocity
    
    :param rho_l: the liquid phase density
    :param rho_g: the gas phase density
    :param tube_diam: the tube diameter
    :param g: the gravity acceleration
    :return: an array of the factors for the liquid and the gas phase
    """
    rho_diff = rho_l - rho_g
    fact_l = np.sqrt(rho_l/g/tube_diam/rho_diff)
    fact_g = np.sqrt(rho_g/g/tube_diam/rho_diff)
    
    return np.array([fact_l, fact_g])
   
    
def main():
    
    skipped = 252 # the number of rows to be skipped when reading the csv
    
    trace_data_ccfl = np.loadtxt("singleTubeFloodingCCFL.csv", 
                                 skiprows=skipped, delimiter=",")
    trace_data_noccfl = np.loadtxt("singleTubeFloodingNoCCFL.csv", 
                                   skiprows=skipped, delimiter=",")
    
    # Fixed Values Data
    diam = 0.0254   # Tube diameter [m]
    g = 9.81        # Gravity acceleration [m.s^-2]
   
    # Results from Model with CCFL 
    time = trace_data_ccfl[:,0]
    
    # Phasic Densities [kg.m^-3]
    rho_l = trace_data_ccfl[:,1] # Liquid
    rho_g = trace_data_ccfl[:,2] # Gas

    # Phasic mass flow rate [kg.s^-1]
    m_l = trace_data_ccfl[:,3]
    m_g = trace_data_ccfl[:,4]

    # Superficial velocity [m.s^-1]
    sup_v_l = superficial_velocity(m_l, diam, rho_l) # Liquid
    sup_v_g = superficial_velocity(m_g, diam, rho_g) # Gas
    
    # Scaling Factor to make a dimensionless parameter
    scale_v = nondim_factor(rho_l, rho_g, diam)

    # Dimensionless superficial velocity
    v_l_star = np.abs(sup_v_l) * scale_v[0]  # Liquid
    v_g_star = np.abs(sup_v_g) * scale_v[1]  # Gas

    # 
    
    # Results from Model without CCFL
    time_no = trace_data_noccfl[:,0]
    
    # Phasic densities [kg.m^-3]
    rho_l_no = trace_data_noccfl[:,1]
    rho_g_no = trace_data_noccfl[:,2]

    # Phasic mass flow rate [kg.s^-1]
    m_l_no = trace_data_noccfl[:,3]
    m_g_no = trace_data_noccfl[:,4]

    # Superficial velocity [m.s^-1]
    sup_v_l_no = superficial_velocity(m_l_no, diam, rho_l_no)   # Liquid
    sup_v_g_no = superficial_velocity(m_g_no, diam, rho_g_no)   # Gas
    
    # Scaling Factor to make a dimensionless parameter
    scale_v_no = nondim_factor(rho_l_no, rho_g_no, diam)

    # Dimensionless superficial velocity
    v_l_star_no = np.abs(sup_v_l_no) * scale_v_no[0]
    v_g_star_no = np.abs(sup_v_g_no) * scale_v_no[1]

    # Make the Flooding Plot
    
    # In terms of dimensionless superficial velocity
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    ax1.set_xlim([0, 1])
    ax1.set_ylim([0, 1])
    ax1.scatter(np.sqrt(v_l_star), np.sqrt(v_g_star), c="black", \
                marker="x", label="CCFL")
    ax1.scatter(np.sqrt(v_l_star_no), np.sqrt(v_g_star_no), c="black",\
                facecolors="none",  marker="o", label="No CCFL")
    plt.legend(loc="upper right")
    plt.xlabel(r"$\sqrt{J_L^*}$")
    plt.ylabel(r"$\sqrt{J_G^*}$")
    plt.savefig("CCFLPlotNonDim.png")
    
    # In terms of dimensioned superficial velocity
    fig = plt.figure()
    ax2 = fig.add_subplot(111)
    ax2.set_xlim([0, 15])
    ax2.set_ylim([0, 0.40])
    ax2.scatter(sup_v_g, np.abs(sup_v_l), 
                c="black", marker="x", label="CCFL")
    ax2.scatter(sup_v_g_no, np.abs(sup_v_l_no), 
                c="black", facecolors="none", marker="o", label="No CCFL")
    plt.legend(loc="upper right")
    plt.xlabel(r"$J_G [m.s^{-1}]$")
    plt.ylabel(r"$J_L [m.s^{-1}]$")
    plt.savefig("CCFLPlotDim.png")


if __name__ == "__main__":
    main()
