#!/bin/env python
# -*- coding: utf-8 -*-
# title         : makeBlowdownPlotWall.py
# author        : WD41, LRS/EPFL/PSI
# date          : November 2015
# purpose       : python3 script to make relevant plots of Exercise 3.1
#               : Thermal-Hydraulics - Blowdown 
#               : The effect of Vessel Wal
#               : Nuclear Computational Lab, Autum 2015
# usage         : > python3 makeBlowdownPlotWall.py 
# prereq        : make sure the extracted trace variables exist as csv files
#               : - ./blowdown01.csv
#               : - ./blowdown02.csv
# py_version    : 3.4.3 (Anaconda Python Distribution 2.1.0 (64bit),
#               : https://store.continuum.io/cshop/anaconda
#-----------------------------------------------------------------------------

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

__author__ = "Damar Wicaksono"

# Global matplotlib setting
mpl.rc("legend", fancybox=True)
mpl.rc("legend", fontsize=12)

def plot_backpressure(time_lowbackpress,
                      np_lowbackpress,
                      time_highbackpress,
                      np_highbackpress,
                      plot_title: str,
                      y_label: str, filename: str):
                      
    fig = plt.figure(figsize=(10,8))
    ax1 = fig.add_subplot(111)
    ax1.plot(time_lowbackpress, np_lowbackpress, label=r"Backpressure 1.0 [bar]")
    ax1.plot(time_highbackpress, np_highbackpress, label=r"Backpressure 2.0 [bar]")
    
    plot_decorate(ax1)
    
    plt.grid()
    plt.xlabel("Time [s]")
    plt.ylabel(y_label)
    plt.title(plot_title)
    plt.legend(loc="upper right")
    plt.savefig(filename)

def plot_decorate(axis):
    axis.spines["right"].set_visible(False)
    axis.spines["top"].set_visible(False)
    axis.spines["left"].set_position(("outward", 10))
    axis.spines["bottom"].set_position(("outward", 10))
    
    
def main():
    
    # Read the data
    # CSV with the following: 0 = time [s], 1 = break flow rate [kg/s] 
    # 2 = pressure [Pa], 3 = break velocity [m/s], 
    # 4 = break temperature [K], 5 = collapsed water level [m], 
    # 6 = total mass release [kg]
    blowdown_low = np.loadtxt("./blowdown01.csv", skiprows=2, delimiter=",")
    blowdown_high = np.loadtxt("./blowdown02.csv", skiprows=2, delimiter=",")
    
    # Make break flow plot
    plot_backpressure(blowdown_low[:,0], blowdown_low[:,1],
                      blowdown_high[:,0], blowdown_high[:,1],
                      "Break flow",
                      "Mass flow rate [kg/s])",
                      "breakFlow.png")
                      
    # Make pressure evolution plot
    plot_backpressure(blowdown_low[:,0], blowdown_low[:,2]/1e6,
                      blowdown_high[:,0], blowdown_high[:,2]/1e6,
                      "Pressure at the Top",
                      "Pressure [MPa]",
                      "topPressure.png")
                      
    # Make break velocity evolution plot
    plot_backpressure(blowdown_low[:,0], blowdown_low[:,3],
                      blowdown_high[:,0], blowdown_high[:,3],
                      "Break Velocity",
                      "Velocity [m/s]",
                      "breakVelocity.png")
    
    # Make break temperature evolution plot
    plot_backpressure(blowdown_low[:,0], blowdown_low[:,4],
                      blowdown_high[:,0], blowdown_high[:,4],
                      "Break Temperature",
                      "Temperature [K]",
                      "breakTemperature.png")
    
    # Make collapsed water level evolution plot
    plot_backpressure(blowdown_low[:,0], blowdown_low[:,5],
                      blowdown_high[:,0], blowdown_high[:,5],
                      "Collapsed Water Level",
                      "Water Level [m]",
                      "waterLevel.png")
    
    # Make total mass release evolution plot
    plot_backpressure(blowdown_low[:,0], blowdown_low[:,6],
                      blowdown_high[:,0], blowdown_high[:,6],
                      "Total Mass Release",
                      "Mass [kg]",
                      "breakMassRelease.png")


if __name__ == "__main__":
    main()
