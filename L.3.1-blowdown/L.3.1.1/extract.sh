#!/bin/bash

if [ "$1" == "-h" ] || [ "$1" == "--help" ] ; then
    echo "Usage: `basename $0` <trcxtv.xtv> <tracevars.inp> || `basename $0` clean"
    exit 0
fi



# Extract trace variables
if [ "$1" == "clean" ] ; then
    # Clean previously generated csv files
    files=$(ls *.csv 2> /dev/null | wc -l)
    if [ $files != 0 ] ; then
        files=$(ls *.csv)
        for f in $files
        do
            rm -f $f
        done
    fi
    # Clean previously generated png files
    files=$(ls *.png 2> /dev/null | wc -l)
    if [ $files != 0 ] ; then
        files=$(ls *.png)
        for f in $files
        do
            rm -f $f
        done
    fi

    for f in ".msg" ".out" ".echo" ".dif" ".tpr" ".dmp"
    do
        aux=$2$f
        rm -f $aux
    done
else
    trc=$(echo $1 | cut -f 1 -d ".")
    cat $2 | sed s/%tracin/$trc/ | aptplot_v6.5.2_inst01.sh -batch - -nowin
fi
