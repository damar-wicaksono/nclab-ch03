#!/bin/env python
# -*- coding: utf-8 -*-
# title         : makeBlowdownPlotWall.py
# author        : WD41, LRS/EPFL/PSI
# date          : November 2015
# purpose       : python3 script to make relevant plots of Exercise 3.1
#               : Thermal-Hydraulics - Blowdown 
#               : The effect of Vessel Wal
#               : Nuclear Computational Lab, Autum 2015
# usage         : > python3 makeBlowdownPlotWall.py 
# prereq        : make sure the extracted trace variables exist as csv files
#               : - ../L.3.1.1/blowdown02.csv
#               : - ./blowdown03.csv
#               : - ./blowdown04.csv
# py_version    : 3.4.3 (Anaconda Python Distribution 2.1.0 (64bit),
#               : https://store.continuum.io/cshop/anaconda
#-----------------------------------------------------------------------------

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

__author__ = "Damar Wicaksono"

# Global matplotlib setting
mpl.rc("legend", fancybox=True)
mpl.rc("legend", fontsize=12)

def plot_backpressure(time_top,
                      np_top,
                      time_bottom,
                      np_bottom,
                      time_bottom_wall,
                      np_bottom_wall,
                      plot_title: str,
                      y_label: str, filename: str):
                      
    fig = plt.figure(figsize=(10,8))
    ax1 = fig.add_subplot(111)
    ax1.plot(time_top, np_top, label=r"Top break")
    ax1.plot(time_bottom, np_bottom, label=r"Bottom Break")
    ax1.plot(time_bottom_wall, np_bottom_wall, label=r"Bottom break w/ Wall")
    
    plot_decorate(ax1)
    
    plt.grid()
    plt.xlabel("Time [s]")
    plt.ylabel(y_label)
    plt.title(plot_title)
    plt.legend(loc="upper right")
    plt.savefig(filename)

def plot_decorate(axis):
    axis.spines["right"].set_visible(False)
    axis.spines["top"].set_visible(False)
    axis.spines["left"].set_position(("outward", 10))
    axis.spines["bottom"].set_position(("outward", 10))
    
    
def main():
    
    # Read the data
    # CSV with the following: 0 = time [s], 1 = break flow rate [kg/s] 
    # 2 = pressure [Pa], 3 = break velocity [m/s], 
    # 4 = break temperature [K], 5 = collapsed water level [m], 
    # 6 = total mass release [kg]
    blowdown_top = np.loadtxt("../L.3.1.1/blowdown02.csv", skiprows=2, delimiter=",")
    blowdown_bottom = np.loadtxt("./blowdown03.csv", skiprows=2, delimiter=",")
    blowdown_bottom_wall = np.loadtxt("./blowdown04.csv", skiprows=2, delimiter=",")
    
    # Make break flow plot
    plot_backpressure(blowdown_top[:,0], blowdown_top[:,1],
                      blowdown_bottom[:,0], blowdown_bottom[:,1],
                      blowdown_bottom_wall[:,0], blowdown_bottom_wall[:,1],
                      "Break flow (Backpressure = 2.0 [bar])",
                      "Mass flow rate [kg/s])",
                      "breakFlowWall.png")
                      
    # Make pressure evolution plot
    plot_backpressure(blowdown_top[:,0], blowdown_top[:,2]/1e6,
                      blowdown_bottom[:,0], blowdown_bottom[:,2]/1e6,
                      blowdown_bottom_wall[:,0], blowdown_bottom_wall[:,2]/1e6,
                      "Pressure at the Top (Backpressure = 2.0 [bar])",
                      "Pressure [MPa]",
                      "topPressureWall.png")
                      
if __name__ == "__main__":
    main()
